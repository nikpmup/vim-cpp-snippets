if exists("b:done_vimcppsnippets")
   finish
endif
let b:done_vimcppsnippets = 1

" Expanding the path is not needed on Vim 7.4
if &cp || version >= 704
    finish
endif

" Add pythonx to the python search path if needed (i.e. <= Vim 7.3).
if !has("python") && !has("python3")
   finish
end

" This will fail if UltiSnips is not installed.
try
   call UltiSnips#bootstrap#Bootstrap()
catch /E117/
   finish
endtry


" This should have been set by UltiSnips, otherwise something is wrong.
if !exists("g:_uspy")
   finish
end


" Expand our path
let s:SourcedFile=expand("<sfile>")
exec g:_uspy "import vim, os, sys"
exec g:_uspy "sourced_file = vim.eval('s:SourcedFile')"
exec g:_uspy "sys.path.append(module_path)"
